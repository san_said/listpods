# Stage: build
FROM golang:alpine3.12 AS builder

ENV GO111MODULE=on
RUN mkdir /src /pkg
COPY ./src /src
WORKDIR /src
RUN go build -o /pkg/listpods -i

# Stage: final
FROM alpine:3.12

RUN addgroup -g 1001 -S podsreaders && \
    adduser -u 1001 -S podsreader podsreaders

# User must be defined numerically in order for K8s to evaluate if user is non-root
USER 1001

COPY --from=builder /pkg/listpods /usr/bin/listpods

CMD ["listpods"]