# listpods

This repo contains the complete source and infrastructure code for deploying the `listpods` application to Kubernetes.

## Repo structure

* `Dockerfile` - the Dockerfile to build the container image
* `./src` directory - contains the Golang source code for `listpods`
* `./helm` directory - contains the Helm charts for deploying `listpods` to a Kubernetes cluster. This contains two values files: one for production (`prod.values.yaml`) and one for development `dev.values.yaml`)

## Testing

### Unit tests

To run the unit tests, run the commands below:

```sh
cd ./src
go test
```

## Deploying

### Prerequisites

* helm
* kubectl
* docker
* kind (optional) 

To get started quickly and deploy the Helm chart in a dev environment, run `deploy.sh` in the root of this directory. Your Helm chart will be deployed as `listpods-dev` and you must have a `dev` namespace in your Kubernetes cluster for this to work. You can install `kind` to run a small local Kubernetes cluster in order to test this tool.

Otherwise, please update the `namespace` values in `prod.values.yaml` and `dev.values.yaml` to reflect the appropriate namespaces for your prod and dev environments respectively.

To deploy a build the image yourself and push to your own (private) repo, you can run `docker build -t <tags> . && docker push <tags>` in the root of this directory, then update the `image.repository` values in the Helm values files to reference your private tags.