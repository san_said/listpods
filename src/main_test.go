package main

import (
	"fmt"
	"testing"
)

func TestGetNamespace(t *testing.T) {
	// Testing that namespace fails appropriately
	const namespacePath = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"

	type expectation struct {
		namespace string
		err       error
	}

	expectations := []expectation{
		expectation{
			namespace: "",
			err: fmt.Errorf("Cannot find namespace path %s and POD_NAMESPACE environment"+
				" variable not set. Please make sure you are running this programme from within a"+
				" Kubernetes cluster.", namespacePath),
		},
	}

	for _, expected := range expectations {
		namespace, err := getNamespace()

		if namespace != expected.namespace || err.Error() != expected.err.Error() {
			t.Errorf(`Expected: namespace to be "` + expected.namespace + `" and error to be "` + expected.err.Error() +
				`".  Instead got: namespace is "` + namespace + `" and error is "` + err.Error() + `"`)
		}
	}
}
