package main

import (
	ctx "context"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8s "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func toStderr(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func getClientSet() (*k8s.Clientset, error) {
	// Create in-cluster config
	config, err := rest.InClusterConfig()

	if err != nil {
		return nil, err
	}

	// Create the client
	client, err := k8s.NewForConfig(config)

	return client, err
}

func getNamespace() (string, error) {
	// Get namespace - this method only works if the pod is deployed in cluster
	const namespacePath = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"

	namespaceBytes, err := ioutil.ReadFile(namespacePath)
	namespace := string(namespaceBytes)

	if err != nil {
		var ok bool
		namespace, ok = os.LookupEnv("POD_NAMESPACE")

		if !(ok) {
			err = fmt.Errorf("Cannot find namespace path %s and POD_NAMESPACE environment"+
				" variable not set. Please make sure you are running this programme from within a"+
				" Kubernetes cluster.", namespacePath)
		}
	}

	return namespace, err
}

func getPods(client *k8s.Clientset, namespace string) (*apiv1.PodList, error) {
	pods, err := client.CoreV1().Pods(namespace).List(ctx.TODO(), metav1.ListOptions{})

	return pods, err
}

func main() {
	client, err := getClientSet()
	toStderr(err)

	namespace, err := getNamespace()
	toStderr(err)

	pods, err := getPods(client, namespace)
	toStderr(err)

	currTime := time.Now().Format("01-01-2001 00:00:00")
	header := `Pods in namespace ` + namespace + `:`
	delimiter := strings.Repeat(`-`, len(header))

	fmt.Print(`[` + currTime + `]`)
	fmt.Println(header)
	fmt.Println(delimiter)

	for _, pod := range pods.Items {
		fmt.Println(pod.ObjectMeta.Name)
	}
}
